#!/bin/sh
#
# Back up to NAS restic repo.

# Nonsense for notify-send... If you're not using KDE, or don't want desktop
# notifications, remove these two env vars and the notify-send sections below.
#
# TODO: If your user ID isn't 1000, change it in the DEBUS_SESSION_BUS_ADDRESS.
export DISPLAY=:0.0
export DBUS_SESSION_BUS_ADDRESS=unix:path=/run/user/1000/bus

BACKUP_LOCATION=/path/to/your/nas/mount/point

if [ ! -d $BACKUP_LOCATION ] ; then
    echo "Unable to find $BACKUP_LOCATION"
    exit 1
fi

export RESTIC_REPOSITORY=$BACKUP_LOCATION
export RESTIC_PASSWORD_FILE=$HOME/.config/restic-password

restic $@ \
    --exclude-caches \
    --exclude-file=$HOME/.config/restic-excludes \
    backup ~

if [ $? -ne 0 ] ; then
    notify-send -a "Local Backup" -h "string:desktop-entry:org.kde.konsole" FAILED "NAS backup failed, check the logs."
    exit 1
fi

notify-send -a "Local Backup" -h "string:desktop-entry:org.kde.konsole" Completed "NAS backup has finished."

restic $@ check

restic $@ forget --prune \
    --keep-daily 7 \
    --keep-weekly 4 \
    --keep-monthly 3
