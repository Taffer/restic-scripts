#!/bin/sh
#
# Test all the repos. You should do this regularly (once a month? once a
# quarter?), but be careful about "egress" fees on your cloud storage, if
# your storage provider charges them.

export RESTIC_PASSWORD_FILE=$HOME/.config/restic-password

# Local backups.
#
# TODO: Edit this!
echo "Local backups:"
export RESTIC_REPOSITORY=/path/to/your/local/restic/repository
restic unlock
restic check --read-data

# NAS backups.
#
# TODO: Edit this!
echo "NAS backups:"
export RESTIC_REPOSITORY=/path/to/your/nas/restic/repository
restic unlock
restic check --read-data

# B2 backups.
#
# TODO: Edit this! Put your B2 bucket name and the name you'd like for your
# restic repository (it'll show up as a directory in your B2 bucket).
#
# Set B2_ACCOUNT_ID and B2_ACCOUNT_KEY to your B2 application key ID and the
# B2 application key. See the restic docs for details.
#
# I keep the B2 account key and my restic repo password in files under
# ~/.config that are only readable by me. This isn't secure; keep your
# important/valuable passwords in a password manager.
echo "B2 backups:"
export B2_ACCOUNT_ID=put-your-account-ID-here
export B2_ACCOUNT_KEY=$(cat $HOME/.config/restic-b2-key)
export RESTIC_REPOSITORY=b2:put-your-b2-bucket-name-here:put-your-restic-repository-name-here
restic unlock
restic check --read-data
